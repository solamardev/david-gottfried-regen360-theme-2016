<?php
/**
 * The main template file.
 Template Name: Podcasts
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage SOLAMAR_2014
 * @since solamar 5.0
 * @date 1/2014
 */

get_header(); ?>
<?php
if( is_home() && get_option('page_for_posts') ) {
	$blog_page_id = get_option('page_for_posts');
	echo '<h1 class="blog-title">'.get_page($blog_page_id)->post_title.'...</h1>';

  $the_content = get_page($blog_page_id)->post_content;
  if ( $the_content != '') {
    echo '<div class="entry-content">' . wpautop($the_content) . '</div>';
  }
}
?>
    <div id="primary" class="content-area">
      <div id="content" class="site-content" role="main">

        <header class="entry-header">
          <h1 class="entry-title">Podcasts</h1>
        </header>
        <div class="podcast-wrapper">
          <?php echo get_podcasts_in_table(); ?>

          <div class="display-grid podcast-grid">
          
          <?php

          $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

          $blog_loop = new WP_Query( array( 
            'posts_per_page' => '12',
            'category_name' => 'podcast',
            'paged' => $paged
          ));

          $count = 1;

          if ( $blog_loop->have_posts() ) {
            while ( $blog_loop->have_posts() ) : $blog_loop->the_post(); 

              $blog_id = $post->ID; ?>
              <div class="item">
                <div class="podcast-wrap clearfix">

                <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                  <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
                  </div>
                <?php endif; ?>

                  <div class="title-wrap">
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                  </div>
                  <div class="excerpt-wrap">	
                    <?php the_powerpress_content(); ?>
                    <div class="entry-meta">
                      <?php Solamar_2015_entry_meta(); ?>
                      <?php edit_post_link( __( 'Edit', 'solamar-2014' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-meta -->

                    <?php  the_excerpt(); ?>

                    <div class="read-more"><a class="button" href="<?php echo get_permalink($post->ID); ?>">Listen &raquo;</a></div>
                  </div><!-- excerpt-wrap -->
                </div><!-- podcast-wrap -->
              </div><!-- col-xs-12 col-sm-4 -->

            <?php 
              if ( $count%3 == 0 ) {
              echo '<div class="spacer"></div>';
              }
              $count ++;
            ?>
            <?php endwhile; ?>
          <?php } ?>

          </div><!-- class="display-grid podcast-grid" -->

          <div id="page-navigation" class="navigation">
          <?php
          echo '<div class="nav-previous">' . get_next_posts_link( 'Older Podcasts', $blog_loop->max_num_pages ) . '</div>';
          echo '<div class="nav-next">' . get_previous_posts_link( 'Newer Podcasts' ) . '</div>';
          ?>
          </div><!-- #page-navigation -->

          <?php 
          // clean up after the query and pagination
          wp_reset_postdata(); 
          ?>

        </div><!-- #content -->
      </div><!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>
