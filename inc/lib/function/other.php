<?php 

/* 
 * Gets the very first ancestor post/page
 *******************************************/
function get_top_ancestor($id){
  $current = get_post($id);
  if(!$current->post_parent){
    return $current->ID;
  } else {
    return get_top_ancestor($current->post_parent);
  }
}

?>
