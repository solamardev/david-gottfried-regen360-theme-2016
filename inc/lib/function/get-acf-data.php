<?php

function get_my_acf_data( $format = 'echo' ) {


  // check if the flexible content field has rows of data
  if( have_rows('flex_content') ):

       // loop through the rows of data
      while ( have_rows('flex_content') ) : the_row();

          $output = '';

          if( get_row_layout() == 'four_column_boxes' ):

            $output .= '<section class="four-column-boxes ' . get_sub_field('class') . ' wow  fadeIn" data-wow-duration="3s">';
            $output .= '<div class="row four-columns content-container">';
            $output .= '<div class="col-md-3" >';
            $output .= get_sub_field('first_column');
            $output .= '</div>';
            $output .= '<div class="col-md-3" >';
            $output .= get_sub_field('second_column');
            $output .= '</div>';
            $output .= '<div class="col-md-3" >';
            $output .= get_sub_field('third_column');
            $output .= '</div>';
            $output .= '<div class="col-md-3" >';
            $output .= get_sub_field('fourth_column');
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</section>';

          elseif( get_row_layout() == 'three_column_boxes' ):

            $output .= '<section class="three-column-boxes ' . get_sub_field('class') . ' wow  fadeIn" data-wow-duration="3s">';
            $output .= '<div class="row three-columns content-container">';
            $output .= '<div class="col-md-4" >';
            $output .= get_sub_field('left_column');
            $output .= '</div>';
            $output .= '<div class="col-md-4" >';
            $output .= get_sub_field('center_column');
            $output .= '</div>';
            $output .= '<div class="col-md-4" >';
            $output .= get_sub_field('right_column');
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</section>';

          elseif( get_row_layout() == 'two_column_split' ):

            $output .= '<section class="even-split clearfix ' . get_sub_field('class') . ' wow fadeIn" data-wow-duration="3s">';
            $output .= '<div class="row split-columns content-container">';
            $output .= '<div class="col-md-6 ' . get_sub_field('left_column_class') . '" >';
            $output .= get_sub_field('left_column');
            $output .= '</div>';
            $output .= '<div class="col-md-6 ' . get_sub_field('right_column_class') . '" >';
            $output .= get_sub_field('right_column');
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</section>';

          elseif( get_row_layout() == 'one_third_two_third' ):

            $class = get_sub_field('class');
            $left = 'col-md-9';
            $right = 'col-md-3';

            if (strpos($class,'right') !== false) {
              $left = 'col-md-3';
              $right = 'col-md-9';
            }

            $output .= '<section class="clearfix ' . get_sub_field('class') . ' wow fadeIn" data-wow-duration="3s">';
            $output .= '<div class="row content-container">';
            $output .= '<div class="' . $left . ' ' . get_sub_field('left_column_class') . '" >';
            $output .= get_sub_field('left_column');
            $output .= '</div>';
            $output .= '<div class="' . $right . ' ' . get_sub_field('right_column_class') . '" >';
            $output .= get_sub_field('right_column');
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</section>';

          elseif(get_row_layout() == 'tabs'):

            $output .= '<section class="tabs clearfix ' . get_sub_field('class') . ' wow fadeIn" data-wow-duration="3s">';
            $output .= '<div class="content-container">';
            if( have_rows('tab') ):
            

              $output .= '<ul class="nav nav-tabs" role="tablist">';
              while ( have_rows('tab') ) : the_row();
                $output .= '<li role="presentation"><a href="#' . get_sub_field('tab_slug') . '" aria-controls="' . get_sub_field('tab_slug') . '" role="tab" data-toggle="tab">' . get_sub_field('tab_title') . '</a></li>';
              endwhile;
              $output .= '</ul>';

              $output .= '<div class="tab-content">';
              while ( have_rows('tab') ) : the_row();
                $output .= '<div role="tabpanel" class="tab-pane" id="' . get_sub_field('tab_slug') . '">';
                $output .= get_sub_field('tab_content');
                $output .= '</div>';
              endwhile;
              $output .= '</div>';
              $output .= '</div>';

            endif; 
            $output .= '</section>';

          elseif(get_row_layout() == 'full_column'):

            $output .= '<section class="row ' . get_sub_field('class') . ' wow  fadeIn" data-wow-duration="3s" >';
            $output .= '<div class="content-container">';
            $output .= '<div class="col-md-12 full-column">';
            $output .= get_sub_field('full_column_content');
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</section>';

          elseif(get_row_layout() == 'spacer'):

            $output .= '<section class="row ' . get_sub_field('spacer') . '" >';
            $output .= '</section>';

          endif;
          if ( $format == 'echo' ) {
            echo $output;
          } else {
            return $output;
          }

      endwhile;

  else :

    // no layouts found
    return false;

  endif;
}
?>
