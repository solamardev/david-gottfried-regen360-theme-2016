<?php

/**
 * Replacement for wp_page_menu() 
 *  removes clutter from class lists
 *  replaces current and parent classes with active and open
 *
 * solamar-2012_page_menu()
 *
 * @since solamar-2012 2.0
 *
 * Display or retrieve list of pages with optional home link.
 *
 * The arguments are listed below and part of the arguments are for 
 * {@link wp_list_pages()} function. Check that function for more info on those arguments.
 *
 * sort_column - How to sort the list of pages. Defaults to page title. Use column for posts table.
 * menu_class - Class to use for the div ID which contains the page list. Defaults to 'menu'.
 * echo - Whether to echo list or return it. Defaults to echo.
 * link_before - Text before show_home argument text.
 * link_after - Text after show_home argument text.
 * show_home - If you set this argument, then it will display the link to the home page. The show_home argument really just needs to be set to the value of the text of the link.
 *
 * @param array|string $args
 */
function solamar-2012_page_menu( $args = array() ) {

	$defaults = array('sort_column' => 'menu_order, post_title', 'menu_class' => 'menu', 'echo' => true, 'link_before' => '', 'link_after' => '');

	$args = wp_parse_args( $args, $defaults );

	$args = apply_filters( 'wp_page_menu_args', $args );

	$menu = '';

	$list_args = $args;

	// Show Home in the menu
	if ( ! empty($args['show_home']) ) {

		if ( true === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] )
			$text = __('Home');
		else
			$text = $args['show_home'];

		$class = '';

		if ( is_front_page() && !is_paged() )
			$class = 'class="nav-home active"';

		$menu .= '<li ' . $class . '><a href="' . home_url( '/' ) . '" title="' . esc_attr($text) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';

		// If the front page is a page, add it to the exclude list
		if (get_option('show_on_front') == 'page') {
			if ( !empty( $list_args['exclude'] ) ) {
				$list_args['exclude'] .= ',';
			} else {
				$list_args['exclude'] = '';
			}
			$list_args['exclude'] .= get_option('page_on_front');
		}
	}

	$list_args['echo'] = false;

	$list_args['title_li'] = '';

	$menu .= str_replace( array( "\r", "\n", "\t" ), '', solamar-2012_list_pages($list_args) );

	if ( $menu )
		$menu = '<ul>' . $menu . '</ul>';

	$menu = '<div class="' . esc_attr($args['menu_class']) . '">' . $menu . "</div>\n";

	$menu = apply_filters( 'wp_page_menu', $menu, $args );

	if ( $args['echo'] )
		echo $menu;
	else
		return $menu;
}

/**
 * Retrieve or display list of pages in list (li) format.
 *
 * @since 1.5.0
 *
 * @param array|string $args Optional. Override default arguments.
 * @return string HTML content, if not displaying.
 */
function solamar-2012_list_pages($args = '') {
	$defaults = array(
		'depth' => 0, 'show_date' => '',
		'date_format' => get_option('date_format'),
		'child_of' => 0, 'exclude' => '',
		'title_li' => __('Pages'), 'echo' => 1,
		'authors' => '', 'sort_column' => 'menu_order, post_title',
		'link_before' => '', 'link_after' => '', 'walker' => '',
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	$output = '';
	$current_page = 0;

	// sanitize, mostly to keep spaces out
	$r['exclude'] = preg_replace('/[^0-9,]/', '', $r['exclude']);

	// Allow plugins to filter an array of excluded pages (but don't put a nullstring into the array)
	$exclude_array = ( $r['exclude'] ) ? explode(',', $r['exclude']) : array();
	$r['exclude'] = implode( ',', apply_filters('wp_list_pages_excludes', $exclude_array) );

	// Query pages.
	$r['hierarchical'] = 0;
	$pages = get_pages($r);

	if ( !empty($pages) ) {
		if ( $r['title_li'] )
			$output .= '<li class="pagenav">' . $r['title_li'] . '<ul>';

		global $wp_query;
		if ( is_page() || is_attachment() || $wp_query->is_posts_page )
			$current_page = $wp_query->get_queried_object_id();
		$output .= solamar-2012_walk_page_tree($pages, $r['depth'], $current_page, $r);

		if ( $r['title_li'] )
			$output .= '</ul></li>';
	}

	$output = apply_filters('wp_list_pages', $output, $r);

	if ( $r['echo'] )
		echo $output;
	else
		return $output;
}

/**
 * Retrieve HTML list content for page list.
 *
 * @uses Walker_Page to create HTML list content.
 * @since 2.1.0
 * @see Walker_Page::walk() for parameters and return description.
 */
function solamar-2012_walk_page_tree($pages, $depth, $current_page, $r) {
	if ( empty($r['walker']) )
		$walker = new Solamar_Walker_Page;
	else
		$walker = $r['walker'];

	$args = array($pages, $depth, $r, $current_page);
	return call_user_func_array(array(&$walker, 'walk'), $args);
}
/**
 * Create HTML list of pages.
 *
 * @package WordPress
 * @since 2.1.0
 * @uses Walker
 */
class Solamar_Walker_Page extends Walker_Page {
	/**
	 * @see Walker::start_el()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $page Page data object.
	 * @param int $depth Depth of page. Used for padding.
	 * @param int $current_page Page ID.
	 * @param array $args
	 */
	function start_el(&$output, $page, $depth, $args, $current_page) {
		if ( $depth )
			$indent = str_repeat("\t", $depth);
		else
			$indent = '';

		extract($args, EXTR_SKIP);

    // get the page name
    $page_name = $page->post_name;

    // replace spaces with dashes to emulate page slug
    $page_name = str_replace(' ','-',$page_name);

    // strip out everything but alphanumeric characters and dashes
    $page_name = preg_replace("/[^a-zA-Z0-9-\s]/", "",$page_name);

		$css_class = array('nav-' . $page_name);
		if ( !empty($current_page) ) {

			$_current_page = get_page( $current_page );

			_get_post_ancestors($_current_page);

			if ( isset($_current_page->ancestors) && in_array($page->ID, (array) $_current_page->ancestors) )
				$css_class[] = 'open';

			if ( $page->ID == $current_page )
				$css_class[] = 'active';
		//	elseif ( $_current_page && $page->ID == $_current_page->post_parent )
		//		$css_class[] = 'parent';

		} elseif ( $page->ID == get_option('page_for_posts') ) {
			$css_class[] = 'post-parent';
		}

		$css_class = implode(' ', apply_filters('page_css_class', $css_class, $page));

    // this is commented out as we currently do not have a need for IDs in the menu.
    // toss 'navid-' in front of the slug to ensure uniqueness from rest of site 
		//$css_id = 'navid-' . $page->post_name;
    //$id = strlen( $css_id ) ? ' id="' . esc_attr( $css_id ) . '"' : '';

		$output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '" title="' . esc_attr( wp_strip_all_tags( apply_filters( 'the_title', $page->post_title, $page->ID ) ) ) . '">' . $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after . '</a>';

		if ( !empty($show_date) ) {
			if ( 'modified' == $show_date )
				$time = $page->post_modified;
			else
				$time = $page->post_date;

			$output .= " " . mysql2date($date_format, $time);
		}
	}
}
?>
