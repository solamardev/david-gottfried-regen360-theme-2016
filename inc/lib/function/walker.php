<?php
/*
 * Custom walker for main navigation
 * - insert page slug into LI ID
 * - strip out bloated WP generated classes for LIs
 ***********************************************************/
class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
  function start_el(&$output, $item, $depth, $args) {
    global $wp_query;
    global $post;

    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    /* 
     * Ok, getting rid of completely useless bloated classes 
     * that WP generates for mostly legacy reasons and replace 
     * only what is necessary.  
     */

    // pull the WP generated class list for filtering
    $class_list = empty( $item->classes ) ? array() : (array) $item->classes;

    // create a new array to replace WP generated class list
    $new_class_list = array();

    // run some filtering on the WP generated class list
    if ( $class_list ) {
      foreach( $class_list as $bloat ) {

        // Replace old legacy stuff with simpler 'active' into new classlist array
        if ( $bloat == 'current_page_item' )
          $new_class_list[] = 'active';

        // Replace old legacy stuff with simpler 'open' into new classlist array
        if ( $bloat == 'current_page_ancestor' || $bloat == 'current-menu-ancestor' )
          $new_class_list[] = 'open';
      }
    } else {
      // If there is no class list, then we have to manually insert 'active' and 'open'
      if ( is_page( $item->post_name ) ) {
        $new_class_list[] = 'active';
      }

      // pull all the children pages
      $children = get_children('post_type=page&numberposts=-1');

      $is_parent = false;

      // sift through each child page and see if the we are currently on a child 
      // if so, then we add 'open' to class list of the parent so that we can identify the 'section' we are in
      foreach($children as $child) {
        if ( $item->ID == $post->post_parent ) { $is_parent = true; }
      }
      if($is_parent) { 
        $new_class_list[] = 'open';
      }
    }

    /* 
     * Create page slug for insertion into 'class' 
     *
     *  pull the title from item
     *  - if the Primary Navigation is set in the Menu admin page, then the page title is $item->title
     *  - if the Primary Navigation is NOT set in the Menu admin page, then the page title is $item->post_title
     */

    // check to see if this is a page item (aka no menu was created) 
    $link = '';
    $is_page = false;

    if ( $item->post_type == 'page' ) {
      $link = get_permalink( $item->ID );
      $is_page = true;
    } 

    // checking to see if this is a page or a post
    if ($is_page) {
      $title = $item->post_title;
    } else {
      $title = $item->title;
    }

    // strip out everything but alphanumeric characters and dashes
    $page_name = preg_replace("/[^a-zA-Z0-9-\s]/", "",$page_name);

    // set to all lower case for standardization...leaving case manipulation to CSS
    $page_name = strtolower($title);

    // replace spaces with dashes to emulate page slug
    $page_name = str_replace(' ','-',$page_name);

    // toss 'nav-' in front of the slug to ensure uniqueness from rest of site BUT NOT other menu items
    $page_name = 'nav-' . $page_name;

    $classes = empty( $new_class_list ) ? array() : $new_class_list;
    
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) ); 

    // toss the slug in as the first class then follow up with generated class names
    $class_names = ' class="' . $page_name . ' ' . esc_attr( $class_names ) . '"'; 

    // this is commented out as we currently do not have a need for IDs in the menu.
    // toss 'navid-' in front of the slug to ensure uniqueness from rest of site 
    //$page_name_id = 'navid-' . $page_name;
    //$id = apply_filters( 'nav_menu_item_id', $page_name_id, $item, $args );
    //$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $value . $class_names .'>';

    if ($is_page) {
      $attributes  = ! empty( $title ) ? ' class="' . esc_attr( $item->post_name ) .'" title="'  . esc_attr( $title ) .'"' : '';
      $attributes .= ! empty( $link ) ? ' href="' . esc_attr( $link ) .'"' : '';
    } else {
      $attributes  = ! empty( $item->attr_title ) ? ' class="'  . esc_attr( $item->attr_title ) .'" title="'  . esc_attr( $item->attr_title ) .'"' : '';
      $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
      $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
      $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $title, $item->ID ) . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

	/**
	 * @see Walker::end_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Page data object. Not used.
	 * @param int $depth Depth of page. Not Used.
	 */
	function end_el(&$output, $item, $depth) {
		$output .= "</li>\n";
	}
}

?>
