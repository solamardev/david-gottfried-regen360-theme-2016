<?php
/**
 * Bread crumb output
 *
 * @since Solamar_2015 4.0

  Basic usage:
  echo get_breadcrumbs( $post );
  //returns: <p><a href='{post_url}'>{post_title}</a> &gt; <a href='{post_url}'>{post_title}</a></p>

  Advanced usage:
  echo get_breadcrumbs( $post, array( 'before_all' => '<ol>', 'after_all' => '</ol>', 'before_each' => '<li>', 'after_each' => '</li>', 'separator' => '' ) );
  //returns: <ol><li><a href='{post_url}'>{post_title}</a></li><li><a href='{post_url}'>{post_title}</a></li></ol>

 *************************************************/
function get_breadcrumbs( $starting_page, $deco = array( 'before_all' => '<nav class="sola-breadcrumbs">', 'after_all' => '</nav>', 'before_each' => '', 'after_each' => '', 'separator' => '<span class="pipe">&gt;</span>' ) ) {

  //get our "decorations"
  extract( $deco );

  // get the current post ID
  $post_id = $starting_page->ID;

  //reverse it so the highest (most-parent?) page is first
  $ids = array_reverse( _get_breadcrumbs( $post ) );

  $id_count = count($ids);

  //if only one id, there are no parents. show nothing
  if ( count( $ids ) <= 1 ) return '';

  //loop through each, create decorated links
  $links = array();

  // add the home link
  $links[] = "$before_each<a href='" . get_option('home') . "'>Home</a> $after_each";

  $i = 1;
  foreach ( $ids as $url => $title ) {
    if ( $i != $id_count ) {

      $links[] = "$before_each<a href='$url'>$title</a>$after_each";

    } else {

      $links[] = "$before_each<span class='current-page-title'>$title</span>$after_each";

    }

    $i++;

  }
  //return it all together
  return $before_all . implode( $separator, $links ) . $after_all;
}

  //recursive function for getting all parent, grandparent, etc. IDs
  //not intended for direct use
function _get_breadcrumbs( $starting_page, $container = array() ) {

  //make sure you're working with an object
  $sp = ( ! is_object( $starting_page ) ) ? get_post( $starting_page ) : $starting_page;

  //make sure to insert starting page only once
  if ( ! in_array( get_permalink( $sp->ID ), $container ) )
  $container[ get_permalink( $sp->ID ) ] = get_the_title( $sp->ID );

  //if parent, recursion!
  if ( $sp->post_parent > 0 ) {
    $container[ get_permalink( $sp->post_parent ) ] = get_the_title( $sp->post_parent );
    $container = _get_breadcrumbs( $sp->post_parent, $container );
  }

  return $container;
}

?>
