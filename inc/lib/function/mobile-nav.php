<?
/* this file is depreciated in favor of something better - responsive navigation */
/* function to generate a dropdown select list for mobile devices */

function insert_mobile_nav( $name ) {
  // Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
  // This code based on wp_nav_menu's code to get Menu ID from menu slug

  $menu_name;

  if ( $name != null ) {
    $menu_name = $name;
  }

  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {

    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $numItems = count($menu_items);
    $i = 0;

    $include_pages = '';

    foreach ( (array) $menu_items as $key => $menu_item ) {

      $page_id = $menu_item->object_id;
      //echo $page_id;

      if(++$i === $numItems) {
        $include_pages .= $page_id;
      } else {
        $include_pages .= $page_id . ', ';
      }
    }

    $drop_down_args =  array(
      'depth' => 1,
      'echo' => 0,
      'name' => 'page_id',
      'include' => $include_pages,
      'sort_column'  => 'ID',
      'post_type' => 'page'
    );
  }
  // $menu_list now ready to outpu
  $output = '';

  $output .= '<div id="mobile_nav">';
  $output .= '<form action="' . get_bloginfo("url") . '" method="get">';
  $output .= wp_dropdown_pages( $drop_down_args );
  $output .= '</form>';
  $output .= '<script>';
  $output .= '$(\'#mobile_nav select\').prepend( \'<option name="navigation" value="4" selected>Navigation</option\');';
  $output .= '$(\'#mobile_nav select\').attr( \'onchange\', \'this.form.submit()\');';
  $output .= '</script>';
  $output .= '</div>';

  return $output;
}
