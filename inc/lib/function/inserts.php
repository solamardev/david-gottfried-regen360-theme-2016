<?php

/**
 * echo the current post slug into the body tag ID
 *
 * @since solamar 6.0
 * @date 3/2015
 * @param object $post Current post object
 * @return echo of current post slug
 */
function insert_post_slug($post) {

  $slug = '';

  if ($post) {
    if ( is_front_page() ) {
      $slug = 'home';
    } elseif ( is_home() ) {
      $slug = 'blog';
    } else {
      $slug = $post->post_name;
    }

    echo $slug;
  }
}


/*
 * Insert site generator info 
 *************************************************/
  if( ! function_exists( 'insert_site_generator' ) ) :
    function insert_site_generator() {
      $sitegenerator= __('<div id="site-generator">Brand &amp; Design by <a href="http://www.powerreinvention.com/" target="_blank" title="Enlightened Branding">Enlightened Branding</a></div><!-- #site-generator -->') . "\n";
      return $sitegenerator;
    }
  endif;

/*
 * Insert copyright info 
 * - dynamically creates current year 
 *************************************************/
  if( ! function_exists( 'insert_copyright' ) ) :
    function insert_copyright() {
      $currentyear = date('Y');
      $thisyear = '';
      if($currentyear != 2011) { 
        $thisyear = ' - ' . date('Y') . ' ';
      }
      $copyright = __('<div id="site-copyright">&copy; 2011 ') . $thisyear . __(get_bloginfo('name') .'.  All rights reserved.</div><!-- #site-copywrite -->') . "\n";
      return $copyright;
    }
  endif;

/*
 * Post Loop inserts
 * single function to insert content post loop on any given page
 * output page determined by slugname
 * - Empty for now - insert slug name and content for post loop insert
 *************************************************/
function add_after_loop() {
  global $post;
  $page = get_page($post->ID);
  $pagename = $page->post_name;
  if($pagename == 'XX') {
    echo '';
  } else {
    echo '';
  }
}
add_action('loop_end', 'add_after_loop');

/*
 * Share this functionality
 * - twitter
 * - facebook
 *************************************************/
function Solamar_2015_insert_share_icons() {
  $thispost = get_post($post->ID);
  $thispostlink = get_permalink();
  echo '<section class="post-social-icons">
    <div class="fb-like" data-href="' . rawurlencode(get_permalink()) .'" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false"></div>
    <!-- a href="https://twitter.com/lindapjones" class="twitter-follow-button" data-show-count="false" data-lang="en">Follow @lindapjones</a-->
    <a href="https://twitter.com/share" class="twitter-share-button" data-url="' . rawurlencode(get_permalink()) .'" data-lang="en">Tweet</a>
    <div class="g-plusone" data-size="medium" data-href="' . rawurlencode(get_permalink()) .'"></div>
  </section>';
}

/* implementation

<section class="entry-meta">
  <?php // echo Solamar_2015_insert_share_icons(); ?>
</section><!-- .entry-meta -->

*/

/*
 * facebook hook placed into header.php
 */
function Solamar_2015_fb_hook() {
  do_action('solamar_fb_hook');
}
function Solamar_2015_insert_fb_scripts() {
echo '
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=216169635109725";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, \'script\', \'facebook-jssdk\'));</script>
  ';
}
add_action('solamar_fb_hook','solamar_insert_fb_scripts');

/*
 * twitter hook placed into footer.php
 */
function Solamar_2015_twitter_hook() {
  do_action('solamar_twitter_hook');
}
function Solamar_2015_insert_twitter_scripts() {
  echo '
  <!-- twitter script -->
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  ';
}
add_action('solamar_twitter_hook','solamar_insert_twitter_scripts');

/*
 * google plus hook placed into footer.php
 */
function Solamar_2015_google_plus_hook() {
  do_action('Solamar_2015_google_plus_hook');
}
function Solamar_2015_insert_google_plus_scripts() {
  echo "
  <!-- google plus script -->
  <script type='text/javascript'>
  (function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
  </script>
  ";
}
add_action('Solamar_2015_google_plus_hook','Solamar_2015_insert_google_plus_scripts');

/*
 * Facebook Like this functionality
 * - Like current post 
 *************************************************/
function Solamar_2015_insert_like_button() {
  $thispost = get_post($post->ID);
  $thispostlink = get_permalink();
  echo '<iframe src="http://www.facebook.com/plugins/like.php?href=' . rawurlencode(get_permalink()) . '" scrolling="no" frameborder="0" style="height: 62px; width: 300px" allowTransparency="true"></iframe>';
}

/* 
 * Google + button with number of pluses 
 *************************************************/
if ( ! is_admin() ) {
  function Solamar_2015_insert_google_plus_button() {
    echo '<div class="post-plus-button"><g:plusone></g:plusone></div>';
  }
  function Solamar_2015_add_google_script() {
    wp_enqueue_script( 'solamar-google-api', 'https://apis.google.com/js/plusone.js', array( 'jquery' ), '1.1', true );
  }
  add_action('wp_enqueue_scripts', 'Solamar_2015_add_google_script');
}
/* implementation
<!-- Place this function where you want the +1 button to render -->
 */


/* 
 * Twitter button with number of tweets
 *************************************************/
function Solamar_2015_insert_twitter_button() {
  echo '<div class="post-twitter-button"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>';
}


/*
 * Insert RSS feed 
 *************************************************/
function Solamar_2015_insert_rss_feed() {
  echo '<div class="rss-feed-link"><a href="'.get_bloginfo_rss('rss2_url').'" title="rss feed" target="_blank"></a></div>';
}

?>
