<?php

/**
* Page location check
* - used to determine if current page is an ancestor of a specific page 
* @section_id = top parent page
*************************************************/
function is_section_check($section_id) {

  global $post;

  $is_section = false;

  $current_page = $post->ID;

  $anc = get_post_ancestors( $current_page );

  if ( in_array($section_id, $anc ) || $section_id == $current_page ) { 
    $is_section = true;
  }   

  return $is_section;

}

/**
 * Handy little test for whether or not current page is a sub-page 
 *
 * @since Solamar_2015 4.0
 *************************************************/
function is_subpage() {
  global $post;                                 // load details about this page
  if ( is_page() && $post->post_parent ) {      // test to see if the page has a parent
    return $post->post_parent;                  // return the ID of the parent post
  } else {                                      // there is no parent so...
    return false;                               // ...the answer to the question is false
  }
}
?>
