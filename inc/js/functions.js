/**
 * Functionality specific to the Solamar Base Theme
 *
 * Provides helper functions to enhance the theme experience.
 */

( function( $ ) { 
  $("#podcastTable").tablesorter(); 
  //$(".podcast-search").
} )( jQuery );

( function( $ ) {
	var body    = $( 'body' ),
	    _window = $( window );

	/**
	 * Adds a top margin to the footer if the sidebar widget area is higher
	 * than the rest of the page, to help the footer always visually clear
	 * the sidebar.
	 */
	$( function() {
		if ( body.is( '.sidebar' ) ) {
			var sidebar   = $( '#secondary .widget-area' ),
			    secondary = ( 0 === sidebar.length ) ? -40 : sidebar.height(),
			    margin    = $( '#tertiary .widget-area' ).height() - $( '#content' ).height() - secondary;

			if ( margin > 0 && _window.innerWidth() > 999 ) {
				$( '#colophon' ).css( 'margin-top', margin + 'px' );
      }
		}
	} );

	/**
	 * Enables menu toggle for small screens.
	 */
	( function() {
		var nav = $( '#site-navigation' ), button, menu;
		if ( ! nav ) {
			return;
    }

		button = nav.find( '.menu-toggle' );
		if ( ! button ) {
			return;
    }

		// Hide button if menu is missing or empty.
		menu = nav.find( '.nav-menu' );
		if ( ! menu || ! menu.children().length ) {
			button.hide();
			return;
		}

		$( '.menu-toggle' ).on( 'click.twentythirteen', function() {
			nav.toggleClass( 'toggled-on' );
		} );
	} )();

	/**
	 * Enables transformicon.
	 */

	var anchor = document.querySelectorAll('button');
    
    [].forEach.call(anchor, function(anchor){
      var open = false;
      anchor.onclick = function(event){
        event.preventDefault();
        if(!open){
          this.classList.add('close');
          open = true;
        }
        else{
          this.classList.remove('close');
          open = false;
        }
      };
    }); 

	/**
	 * Makes "skip to content" link work correctly in IE9 and Chrome for better
	 * accessibility.
	 *
	 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
	 */
	_window.on( 'hashchange.twentythirteen', function() {
		var element = document.getElementById( location.hash.substring( 1 ) );

		if ( element ) {
			if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) {
				element.tabIndex = -1;
      }

			element.focus();
		}
	} );

  //function onAfter(curr, next, opts, fwd){
  function onAfter(){

    //get the height of the current slide
    var $ht = $(this).height();

    //set the container's height to that of the current slide
    $(this).parent().animate({height: $ht});
  }

  var testimonialSlides = $('#solamar-testimonial-slideshow');

  testimonialSlides.cycle({
    timeout: 8000,
    speed: 2000,
    fx:'fade',
    width:null,
    after: onAfter,
    cleartypeNoBg:true,
    containerResize:0
  });

} )( jQuery );
