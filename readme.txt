= SOLAMAR THEME 2015 =

* by the Solamar team, http://Solamaragency.com/

== ABOUT THE SOLAMAR THEME ==
This is a custom wordpress theme that follows WordPress development standards and has a few bells and whistles.  It's designed to be installed with the Solamar custom plugin.




== Components structure ==

SCSS:

All scss files are in /inc/scss/

BOWER:
Bower componenets are in bower_components
Currently utilizing:
- bootstrap
- font-awesome
- jquery is there but not referred to.  We are sticking with what is shipped with WordPress



== Task Runner (aka Codekit, Grunt or any other pre-processor) == 

Please have /inc/scss/style.scss to compile as follows:
- compressed
- output to /style.css

Please have /inc/js/functions.js compile as follows:
- compressed
- output to /inc/js/min/functions-min.js



=== Changelog ===

=6.0=

date: 3/20/15

generally clean source code
generally reduce code base
add bootstrap 3.3.2
add Sass
add bourbon
add font-awesome
It's Sassy baby :)

=5.0=
Minor updates to 4.0 and reset for 2014

=4.0=
Convert WP twentythirteen theme to new Solamar 4.0 and integrate bootstrap

=3.1.2=
added FB OG meta tags

=3.1.1=
added mobile navigation functionality - inc/lib/function/mobile-nav.php

= 3.1 =
adjust #primary and #secondary containers to remove strange margins and equalize padding
Solafonts

= 3.0 =
added bootstrap

= 2.1 =

added default color for input placeholders 
added current-menu-ancestor to bloat strip - setting LI with this class to open
social media widget and sidebar on by default - move to plugin and convert to icons
I put an example wp-config.php file into theme for easy reference
add full page page template
set the sales page template to have no header
removed showcase and all post type formats (link, aside, gallery, image, etc...)
registered a blog sidebar, added a blog sidebar-blog.php template file and make sure all blog related templates file call that
removed the search from the header
sidebar.php should not have any default widgets - aka blank
completely reworked the css to adhere to the agreed upon paramater listing.

= 2.0 =
Initial rebuild to combine WP twentyeleven theme with html5 boilerplate
