<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'solamar-2015' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'solamar-2015' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'solamar-2015' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'solamar-2015' ), get_the_date( _x( 'Y', 'yearly archives date format', 'solamar-2015' ) ) );
					else :
						_e( 'Archives', 'solamar-2015' );
					endif;
				?></h1>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content' ); ?>
			<?php endwhile; ?>

			<?php Solamar_2015_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar('blog'); ?>
<?php get_footer(); ?>
