<?php
/**
 * The sidebar containing the blog widget area.
 *
 * If no active widgets in this sidebar, it will be hidden completely.
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */

if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
	<div id="secondary" class="sidebar-container" role="complementary">
		<div class="widget-area">
			<?php dynamic_sidebar( 'sidebar-blog' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php endif; ?>
