<?php
/* Table sorter functionality to pull all posts with a category of podcast and output in table format that can be sorted
 */

function get_podcasts_in_table() {
  
  $podcasts = new WP_Query(array(
      'post_type' => 'post',
      'cat' => 5,
      'posts_per_page' => -1
  ));

  $output = '';

  if ($podcasts) {
    $output .= '<section class="podcast-search" id="podcast-search-wrap" role="tablist" aria-multiselectable="true">';
    $output .= '<header class="panel-heading" id="headingOne"><h3><a role="button" data-toggle="collapse" data-parent="#podcast-search-wrap" href="#collapseThis" aria-expanded="true" aria-controls="collapseOne">Search Podcasts</a><i role="button" data-toggle="collapse" data-parent="#podcast-search-wrap" href="#collapseThis" aria-expanded="true" aria-controls="collapseOne" class="fa fa-angle-down"></i></h3></header>';
    $output .= '<div class="podcast-search-toggle panel-collapse collapse" id="collapseThis" role="tabpanel" aria-labelledby="headingOne">';
    $output .= '<div class="podcast-search-form">' . get_search_form(false) . '</div>';
    $output .= '<table id="podcastTable" class="tablesorter">';
    $output .= '<thead>'; 
    $output .= '<tr>'; 
    $output .= '<th><strong>Sort by:</strong> <span class="button">First Name</span></th>'; 
    $output .= '<th><span class="button">Last Name</span></th>'; 
    $output .= '<th><span class="button">Company</span></th>'; 
    $output .= '</tr>'; 
    $output .= '</thead>'; 
    $output .= '<tbody>'; 

    while ( $podcasts->have_posts() ) : $podcasts->the_post();
      if ( get_field('first_name') != '' ) {
        $post_id = get_the_ID();
        $permalink = get_permalink($post_id);
        $output .= '<tr>'; 
        $output .= '<td class="first-name"><a href="' . $permalink . '">' . get_field('first_name') . '</a></td>'; 
        $output .= '<td class="last-name"><a href="' . $permalink . '">' . get_field('last_name') . '</a></td>'; 
        $output .= '<td class="company">';
        if ( get_field('company') ) {
        $output .= '&mdash; <a href="' . $permalink . '">' . get_field('company') . '</a>';
        }
        $output .= '</td>'; 
        $output .= '</tr>'; 
      }

    endwhile;
    $output .= '</tbody>'; 
    $output .= '</table>';
    $output .= '</div>';
    $output .= '</section>';

    return $output;
    
  } else {

    return false;

  }

}

?>
