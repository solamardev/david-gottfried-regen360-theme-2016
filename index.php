<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */

get_header(); ?>
<h1 class="entry-title"><?php next_posts_link('<i class="fa fa-chevron-left"></i>', $the_query->max_num_pages); ?> Features <?php previous_posts_link('<i class="fa fa-chevron-right"></i>', $the_query->max_num_pages); ?></h1>
<div class="sidebar-blog"><?php if ( ! dynamic_sidebar( 'sidebar-blog' ) ) : ?><?php endif; ?></div>
<div class="features-podcast clearfix">
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
        <?php
          $podcast_button = 'Read';
          $cats = get_the_category($post->ID);
          foreach($cats as $cat) {
            if($cat->slug == 'podcast' || $cat->slug == 'interviews') {
              $podcast_button = 'Listen';
            }
          }
        ?>
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); $image = $image[0]; ?>
        <?php else :
        $image = get_bloginfo( 'stylesheet_directory') . '/images/default_feature_img.jpg'; ?>
        <?php endif; ?>
					<div id="feature" class="feature-wrap col-xs-12 col-md-6 col-lg-3" style="background-image: url('<?php echo $image; ?>')" >
			        <div class="feature-overlay">
			        <div class="feature-content">
			              <div class="title-wrap">
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                <p class="entry-date"><?php the_time('F jS, Y') ?></p>
              </div>
              	
                <div class="post-tagline fadeIn"><?php the_excerpt(); ?></div>

                <div class="read-more"><a class="button" href="<?php echo get_permalink($post->ID); ?>"><?php echo $podcast_button; ?></a></div>
              </div><!-- excerpt-wrap -->
         	  </div>	
          </div><!-- col-4 -->
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
		</div>
<?php get_footer(); ?>
