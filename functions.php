<?php
/**
 * Solamar 2015 functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
*/

/**
 * Solamar 2015 only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
	require get_template_directory() . '/inc/back-compat.php';

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Solamar 2015 supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since solamar 6.0
 * @date 3/2015
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_setup() {
	/*
	 * Makes Solamar 2015 available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Solamar 2015, use a find and
	 * replace to change 'Solamar_2015' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'solamar-2015', get_template_directory() . '/languages' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( '/inc/css/editor-style.css', Solamar_2015_fonts_url() ) );

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );
	 */

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Navigation Menu', 'Solamar_2015' ) );

	/*
	 * Set a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 604, 270, true );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

  /**
   * Clean_WP_Header_tags	
   * - remove clutter (aka extra markup) from the header
   *****************************************************/
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'rel_canonical');
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
    remove_action('wp_head', 'index_rel_link');
}
add_action( 'after_setup_theme', 'Solamar_2015_setup' );

/**
 * Returns the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since solamar 6.0
 * @date 3/2015
 * @date 3/2015
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function Solamar_2015_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	 * supported by Source Sans Pro, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'Solamar_2015' );

	if ( 'off' !== $source_sans_pro ) {
		$font_families = array();

		if ( 'off' !== $source_sans_pro )
			$font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/**
 * Enqueues scripts and styles for front end.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_scripts_styles() {
	// Adds JavaScript to pages with the comment form to support sites with
	// threaded comments (when in use).
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Add Open Sans fonts, used in the main stylesheet.
	wp_enqueue_style( 'solamar-fonts', Solamar_2015_fonts_url(), array(), null );

  // Loads Bootstrap goodness
	wp_enqueue_style( 'solamar-bootstrap', get_template_directory_uri() . '/bower_components/bootstrap/css/bootstrap.css', array(), '3.3.4' );

  // Loads Font Awesome
	wp_enqueue_style( 'solamar-font-awesome', get_template_directory_uri() . '/bower_components/font-awesome/css/font-awesome.min.css', array(), '4.3.0' );

	// Loads Animate.css
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/bower_components/animate.css/animate.min.css', array(), null );

	// Loads our main stylesheet.
	wp_enqueue_style( 'solamar-style', get_stylesheet_uri() );

	// Loads the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'solamar-ie', get_template_directory_uri() . '/inc/css/ie.css', array( 'solamar-style' ), '2013-07-18' );
	wp_style_add_data( 'solamar-ie', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'jquery-ui');

	// Loads Bootstrap JavaScript file 
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/bower_components/bootstrap/dist/js/bootstrap.min.js', array( 'jquery' ), '3.3.4', true );

	wp_enqueue_script( 'tablesorter-script', get_template_directory_uri() . '/bower_components/tablesorter/jquery.tablesorter.min.js', array( 'jquery' ), '2.0.5b', true );

	// Loads JavaScript file with functionality specific to Solamar theme 
	wp_enqueue_script( 'solamar-script', get_template_directory_uri() . '/inc/js/min/functions-min.js', array( 'jquery' ), '2013-07-18', true );

}
add_action( 'wp_enqueue_scripts', 'Solamar_2015_scripts_styles' );

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function Solamar_2015_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'Solamar_2015' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'Solamar_2015_wp_title', 10, 2 );

/**
 * Registers two widget areas.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area', 'Solamar_2015' ),
		'id'            => 'sidebar-main',
		'description'   => __( 'Appears in the sidebar section of the site.', 'Solamar_2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Blog Widget Area', 'Solamar_2015' ),
		'id'            => 'sidebar-blog',
		'description'   => __( 'Appears on the blog in the sidebar.', 'Solamar_2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Social Media Widget Sidebar', 'Solamar_2015' ),
		'id' => 'social-media-widget',
		'description' => __( 'The sidebar for the social media widget', 'Solamar_2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Testimonial Sidebar', 'Solamar_2015' ),
		'id' => 'testimonial-widget',
		'description' => __( 'The sidebar for the testimonial widget', 'Solamar_2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'Solamar_2015_widgets_init' );

if ( ! function_exists( 'Solamar_2015_paging_nav' ) ) :
/**
 * Displays navigation to next/previous set of posts when applicable.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'Solamar_2015' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'Solamar_2015' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'Solamar_2015' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'Solamar_2015_post_nav' ) ) :
/**
 * Displays navigation to next/previous post when applicable.
*
* @since solamar 6.0
 * @date 3/2015
*
* @return void
*/
function Solamar_2015_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'Solamar_2015' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'Solamar_2015' ) ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'Solamar_2015' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'Solamar_2015_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own Solamar_2015_entry_meta() to override in a child theme.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post"><i class="fa fa-pencil"></i> ' . __( 'Sticky', 'Solamar_2015' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		Solamar_2015_entry_date();

	if ( comments_open() ) :
			echo '<span class="comments-link"><i class="fa fa-comment-o"></i> ';
				comments_popup_link( '<span class="leave-reply">' . __( '0 comments', 'solamar-2015' ) . '</span>', __( '1 comment', 'solamar-2015' ), __( '% comments', 'solamar-2015' ) );
			echo '</span>';
		endif; // comments_open()

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'Solamar_2015' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links"><i class="fa fa-thumb-tack"></i> ' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'Solamar_2015' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links"><i class="fa fa-tag"></i> ' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author"><i class="fa fa-user"></i> %3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'Solamar_2015' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'Solamar_2015_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own Solamar_2015_entry_date() to override in a child theme.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @param boolean $echo Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function Solamar_2015_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'Solamar_2015' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'Solamar_2015' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

if ( ! function_exists( 'Solamar_2015_the_attached_image' ) ) :
/**
 * Prints the attached image with a link to the next attached image.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return void
 */
function Solamar_2015_the_attached_image() {
	$post                = get_post();
	$attachment_size     = apply_filters( 'Solamar_2015_attachment_size', array( 724, 724 ) );
	$next_attachment_url = wp_get_attachment_url();

	/**
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}

	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

/**
 * Returns the URL from the post.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @return string The Link format URL.
 */
function Solamar_2015_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since solamar 6.0
 * @date 3/2015
 * @date 3/2015
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function Solamar_2015_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'Solamar_2015_body_class' );


/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since solamar 6.0
 * @date 3/2015
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function Solamar_2015_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'Solamar_2015_customize_register' );


/**
 * Binds JavaScript handlers to make Customizer preview reload changes
 * asynchronously.
 *
 * @since solamar 6.0
 * @date 3/2015
 */
function Solamar_2015_customize_preview_js() {
	wp_enqueue_script( 'twentythirteen-customizer', get_template_directory_uri() . '/inc/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
}
add_action( 'customize_preview_init', 'Solamar_2015_customize_preview_js' );


/**
 *
 * SOLAMAR SPECIFIC 
 **************************************************************************/

/**
* Social-network-widget
* - Adds social network widget for placement in sidebars
*****************************************************/
require( get_template_directory() . '/inc/lib/widgets/social-network.php' );

/*
 * Development Dasboard Widget
 * - identifying information for clients to find initial designer and developer
 *************************************************/

function Solamar_2015_wp_dashboard() {
  echo 'This site was designed and developed by Solamar Agency, info@solamarmarketing.com';
}

/* add Dashboard Widgets via function wp_add_dashboard_widget() */
function Solamar_2015_wp_dashboard_setup() {
  wp_add_dashboard_widget( 'Solamar_2015_wp_dashboard', __( 'Design and Development' ), 'Solamar_2015_wp_dashboard' );
}

/* use action hook, to integrate new widget on dashboard */
add_action('wp_dashboard_setup', 'Solamar_2015_wp_dashboard_setup');

/**
 * Booleans
 * - is_section_check($parentname) - is the current page a parent or a child of the given $parentname?
 * - is_subpage() - is the current page a child?
 */
include('inc/lib/function/boolean.php');

/**
 * Other
 * this is other functions that may need a more proper home later on (aka the bucket)
 * get_top_ancestor() - gets you the very top ancestor of a post/page
 */
include('inc/lib/function/other.php');

/**
 *
 * INSERTS
 *  insert_site_generator()
 *  insert_copyright()
 *  add_after_loop()
 *  Solamar_2015_insert_share_icons()
 *  Solamar_2015_insert_like_button()
 *  Solamar_2015_insert_rss_feed()
 * Solamar_2015_breadcrumbs()
 *
 *************************************************/
require(get_template_directory() . '/inc/lib/function/inserts.php');
require(get_template_directory() . '/inc/lib/function/breadcrumb.php');
require(get_template_directory() . '/inc/lib/function/get-acf-data.php');
require(get_template_directory() . '/bower_components/tablesorter/table.php');

/**
* Custom Admin Logo + Link
*****************************************************/
function custom_login_logo() {
    echo '<style type="text/css">'.
             'h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login-logo.png) !important; background-size:200px 44px !important; height: 44px !important; width:200px !important;}'.
         '</style>';
}
add_action( 'login_head', 'custom_login_logo' );


/* Custom Admin Login Header Link */
function custom_login_url() {
    return home_url( '/' );
}
add_filter( 'login_headerurl', 'custom_login_url' );


/* Custom Admin Login Header Link Alt Text */
function custom_login_title() {
    return get_option( 'blogname' );
}
add_filter( 'login_headertitle', 'custom_login_title' );

/**
 * Remove the emoji silliness that the lovelies at Wordpress had added in 2015
 ******************************************************************************/

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// adding SVG to allowable mime-types
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

?>
