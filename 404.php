<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
      <div class="hentry">

        <header class="entry-header">
          <h1 class="entry-title"><?php _e( 'Ooops! That page was not found.', 'solamar-2015' ); ?></h1>
        </header>

        <div class="page-wrapper">
          <div class="page-content">
            <h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'solamar-2015' ); ?></h2>
            <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'solamar-2015' ); ?></p>

            <?php get_search_form(); ?>
          </div><!-- .page-content -->
        </div><!-- .page-wrapper -->

      </div><!-- .hentry -->
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
