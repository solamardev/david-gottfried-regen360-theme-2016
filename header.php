<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */

global $post;

?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8 lte-ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

  <?php if ( function_exists( 'insert_fbog' ) ) { echo insert_fbog($post); } ?>

  <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
  <link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-touch-icon.png">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script src="/wp-content/themes/solamar/bower_components/wow/dist/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>

	<?php wp_head(); ?>

	<!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/inc/js/html5.js"></script>
  <![endif]-->

</head>

<body id="<?php insert_post_slug( $post ); ?>" <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header clearfix" role="banner">
			<div class="container">
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<h1 class="site-title"><span><?php bloginfo( 'name' ); ?></span></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</a>
	
				<div id="navbar" class="navbar">
					
					<nav id="site-navigation" class="navigation main-navigation" role="navigation">
						<button id="lines-button" class="lines-button x menu-toggle" type="button" role="button" aria-label="Toggle Navigation">
						  <span class="lines"></span>
						</button>
						<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'solamar-2015' ); ?>"><?php _e( 'Skip to content', 'solamar-2015' ); ?></a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					</nav><!-- #site-navigation -->
				</div><!-- #navbar -->
				<div class="social-media"><?php if ( ! dynamic_sidebar( 'social-media-widget' ) ) : ?>
						<?php endif; // end sidebar widget area ?></div>
			</div><!-- .container -->
			
			<?php if ( is_page_template('page-sales.php') ) : ?>
    		<div class="hero clearfix" style="background-image: url(<?php the_field('sales_page_banner'); ?>);"></div>
			<?php else : ?>
    		<div class="hero clearfix">
				<div class="left-side-wrap col-sm-4">
					<img src="<?php the_field('left_side_image'); ?>" alt="" />
					<h2 class="box-title"><?php the_field('left_side_title'); ?></h2>
					<div style="text-align:center;"><a class="button" href="<?php the_field('left_side_link'); ?>">More Info</a></div>
				</div>
				<div class="center-wrap col-sm-4">
				<img src="<?php the_field('hero_center_image'); ?>" alt="" />
				</div>
				<div class="right-side-wrap col-sm-4">
					<img src="<?php the_field('right_side_image'); ?>" alt="" />
					<h2 class="box-title"><?php the_field('right_side_title'); ?></h2>
					<div style="text-align:center;"><a class="button" href="<?php the_field('right_side_link'); ?>">More Info</a></div>
			</div>
		<?php endif;  ?>		
			
		</header><!-- #masthead -->
		<div class="opt-in-wrap top">
          <?php
          $that_bit = get_posts( array('post_type' => 'bit', 'pagename' => 'opt-in') ); 
            if ( $that_bit ) {
              foreach ( $that_bit as $abit ) {
                echo wpautop($abit->post_content);
              }
          }
          wp_reset_postdata();
          ?>
				</div>
				<div class="arrow-down"></div>
				<?php 

        // General ACF Content
        if ( is_page_template('page-sales.php') ) :
					get_my_acf_data();
        endif;  

        // Specific addition for Summit Sales Page
        if ( is_page('244') ) {
          echo '<div class="testimonial-master-wrap clearfix">';
            echo '<div class="col-sm-2 col-sm-offset-2 desktop testimonial-tag-wrap">';
              echo '<div class="heart"></div>';
              echo '<div class="testimonial-tag">Praise For David Gottfried</div>';
            echo '</div>';
            echo '<div class="col-xs-12 col-sm-6 testimonial-content-wrap">';
              echo do_shortcode('[soltestimonial category="summit-footer"]');
            echo '</div>';
          echo '</div>';
        }
        ?>

		<div id="main" class="site-main">
