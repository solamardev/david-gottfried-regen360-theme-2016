<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage SOLAMAR_2015
 * @since solamar 6.0
 * @date 3/2015
 */
?>

		</div><!-- #main -->
		<div class="arrow-up"></div>
		<div class="opt-in-wrap bottom">
          <?php
          $that_bit = get_posts( array('post_type' => 'bit', 'pagename' => 'opt-in-footer') ); 
            if ( $that_bit ) {
              foreach ( $that_bit as $abit ) {
                echo wpautop($abit->post_content);
              }
          }
          wp_reset_postdata();
          ?>
				</div>
    <?php if( get_field('show_feature') ): ?>    
    <div class="home-feature clearfix" style="background-image: url(<?php the_field('feature_image'); ?>);">
      <div class="feature-copy-wrap <?php the_field('copy_position'); ?>">
        <div class="feature-copy">
          <?php the_field('feature_copy'); ?>
        </div>
      </div>
    </div>	
    <div class="home-feature mobile">
      <div class="feature-copy-wrap">
        <div class="feature-copy">
          <?php the_field('feature_copy'); ?>
        </div>
      </div>
    </div>  	
  <?php endif; // end home feature area ?>
		<div class="testimonials clearfix">
		<div class="testimonial-tag-wrap col-xs-12 col-md-3 col-lg-2"><div class="heart"></div><div class="testimonial-tag">Praise For David Gottfried</div></div>
		<div class="testimonial-content-wrap col-xs-12 col-md-9 col-lg-10"><?php if ( ! dynamic_sidebar( 'testimonial-widget' ) ) : ?>
    <?php endif; // end testimonial widget area ?></div></div>
		<div class="features">
    <?php

          $blog_loop = new WP_Query( array( 
            'posts_per_page' => '4',
          ));

          while ( $blog_loop->have_posts() ) : $blog_loop->the_post(); 

            $blog_id = $post->ID; ?>
                       <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            $image = $image[0]; ?>
            <?php else :
            $image = get_bloginfo( 'stylesheet_directory') . '/images/default_feature_img.jpg'; ?>
            <?php endif; ?>
            <div id="feature" class="feature-wrap col-xs-12 col-md-6 col-lg-3" style="background-image: url('<?php echo $image; ?>')" onclick="void(0)">
			        <div class="feature-overlay">
			        <div class="feature-content">
			              <div class="title-wrap">
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                <p class="entry-date"><?php the_time('F jS, Y') ?></p>
              </div>
              	
                <div class="post-tagline fadeIn"><?php the_excerpt(); ?></div>

                <div class="read-more"><a class="button" href="<?php echo get_permalink($post->ID); ?>">Listen</a></div>
              </div><!-- excerpt-wrap -->
         	  </div>	
            </div><!-- col-4 -->

          <?php endwhile; ?>
      </div>
    <?php if ( is_front_page() ) { ?>
    <section class="podcast-wrapper clearfix">
      <div class="site-main">
        <header>
          <h1 class="entry-title">Latest Podcasts</h1>
        </header>
        <div class="display-grid podcast-grid">
        <?php
        $blog_loop = new WP_Query( array( 
          'posts_per_page' => '3',
          'category_name' => 'podcast'
        ));

        if ( $blog_loop->have_posts() ) {
          while ( $blog_loop->have_posts() ) : $blog_loop->the_post(); 

            $blog_id = $post->ID; ?>
            <div class="item">
              <div class="podcast-wrap clearfix">

              <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                <div class="entry-thumbnail">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
                </div>
              <?php endif; ?>

                <div class="title-wrap">
                  <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                </div>
                <div class="excerpt-wrap">	
                  <?php the_powerpress_content(); ?>
                  <div class="entry-meta">
                    <?php solamar_2015_entry_meta(); ?>
                  </div><!-- .entry-meta -->

                  <?php  the_excerpt(); ?>

                  <div class="read-more"><a class="button" href="<?php echo get_permalink($post->id); ?>">listen &raquo;</a></div>
                </div><!-- excerpt-wrap -->
              </div><!-- podcast-wrap -->
            </div><!-- col-xs-12 col-sm-4 -->

          <?php endwhile; ?>
        <?php } ?>

        </div><!-- row-->
      </div><!-- site-main  -->
    </section>
    <?php } ?>
    <footer id="colophon" class="site-footer clearfix" role="contentinfo">
			<div class="wrap clearfix">
        <div class="footer-logo"></div>
        <div class="social-media"><?php if ( ! dynamic_sidebar( 'social-media-widget' ) ) : ?>
              <?php endif; // end sidebar widget area ?></div>
        <div class="site-info container">
          <?php do_action( 'Solamar_2015_credits' ); ?>
          &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.<br/><a href="<?php echo esc_url( __( 'http://solamaragency.com/', 'solamar-2015' ) ); ?>" title="<?php esc_attr_e( 'Full Service Marketing Agency | Custom Web Design | Branding', 'solamar-2015' ); ?>" target="_blank"><?php printf( __( 'Site designed by %s', 'solamar-2015' ), 'Solamar Agency' ); ?>.</a>
        </div><!-- .site-info -->
			</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>

  <!-- fixes for ie :) -->
  <!--[if lt IE 10]>
  <script src="<?php echo get_template_directory_uri(); ?>/inc/js/jquery.placeholder.1.3.min.js" type="text/javascript"></script>
  <script>$(document).ready(function($){ $.Placeholder.init(); });</script>
  <![endif]-->

<?php if ( is_page('summit') ) { ?>
<div class="speaker-bio-modals">
<?php
  $speaker_bio_modal_bit = new WP_Query( array('post_type' => 'bit', 'pagename' => 'speaker-bio-modals') ); 
  if ( $speaker_bio_modal_bit ) {
    while ( $speaker_bio_modal_bit->have_posts() ) : $speaker_bio_modal_bit->the_post();
      the_content();
    endwhile;
  }
?>
</div>
<?php } ?>
</body>
</html>
